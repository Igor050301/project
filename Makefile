CC = $(shell which g++)
TARGET = counterProject
SRCPREFIX = src/
OBJPREFIX = obj/

ifndef CC
$(shell sudo apt-get install g++)

else
$(TARGET) : $(OBJPREFIX)main.o $(OBJPREFIX)counterOfWords.o
	$(CC) $(OBJPREFIX)main.o $(OBJPREFIX)counterOfWords.o -o $(TARGET)
	./$(TARGET)

$(OBJPREFIX)main.o : $(SRCPREFIX)main.cpp
	$(CC) -c $(SRCPREFIX)main.cpp -o $(OBJPREFIX)main.o

$(OBJPREFIX)counterOfWords.o : $(SRCPREFIX)counterOfWords.cpp
	$(CC) -c $(SRCPREFIX)counterOfWords.cpp -o $(OBJPREFIX)counterOfWords.o
endif

clean:
	rm $(TARGET) || true

install: 
	$(CC) $(SRCPREFIX)main.cpp $(SRCPREFIX)counterOfWords.cpp -o $(TARGET)
