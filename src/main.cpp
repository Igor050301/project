#include <iostream>
#include <string>
#include "counterOfWords.h"


int main(int argc, const char * argv[]){
    setlocale(LC_ALL, "RU");
    std::string str = "";
    std::cout << "Enter the string" << std::endl;
    std::getline(std::cin, str);
    //std::cout << "Entered string: " << str << std::endl;
    std::cout << "Count of words: " << countOfWords(str) << std::endl;
    //std::cout << "String - result: " << str << std::endl;
    return 0;
}
